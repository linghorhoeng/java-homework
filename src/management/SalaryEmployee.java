package management;

public class SalaryEmployee extends StaffMember implements Comparable<StaffMember> {
    private double salary;
    private double bonus;
    public SalaryEmployee(int id, String name, String address, double salary, double bonus) {
        super(id, name, address);
        this.salary=salary;
        this.bonus=bonus;
    }
    public void setSalary(double salary){
        this.salary=salary;
    }
    public void setBonus(double bonus){
        this.bonus=bonus;
    }
    public  double getSalary(){
        return salary;
    }
    public double getBonus(){
        return bonus;
    }


    @Override
    public double pay() {
        return salary+bonus;
    }

    public String toString(){
    return "ID:"+id+"\n"+"Name:"+name+"\n"+"Address:"+address+"\n"
            +"Salary:"+salary+"\n"+"Bonus:"+bonus+"\n"+ "Payment"+pay()+"\n"+
            "=====================================================";
}
    @Override
    public int compareTo(StaffMember o) {
        return name.compareTo(o.name);
    }
}
