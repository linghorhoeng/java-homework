package management;


public abstract class StaffMember implements Comparable<StaffMember>  {
    protected int id;
    protected String name;
    protected String address;

    public StaffMember(int id, String name, String address) {
        this.id = id;
        this.name = name;
        this.address = address;
    }

    public void setId(int id){ this.id=id; }
    public void setName(String name){this.name=name; }
    public void setAddress(String address){ this.address=address; }
    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getAddress() {
        return address;
    }

    //    abstract method
    public abstract double pay() ;

    public String toString(){
        //overriding the toString() method
        return "ID:"+id+"\n"+"Name:"+name+"\n"+"Address:"+address+"\n"
                +"Thanks"+"\n"+
                "====================================================";
    }
    @Override
    public int compareTo(StaffMember o) {
        return name.compareTo(o.name);
    }
}