package management;

public class HourlyEmployee extends StaffMember implements Comparable<StaffMember>{
    private int hourworked ;
    private double rate;

    public HourlyEmployee(int id, String name, String address, int hourworked, double rate) {
        super(id, name, address);
        this.hourworked=hourworked;
        this.rate=rate;
    }
    public void setHourworked(int hourworked){
        this.hourworked=hourworked;
    }
    public void setRate(double rate){
        this.rate=rate;
    }
    public  int getHourworked(){
        return hourworked;
    }
    public double getRate(){
        return rate ;
    }


    @Override
    public double pay() {
        return hourworked*rate;
    }

    public String toString(){
        return "ID:"+id+"\n"+"Name:"+name+"\n"+"Address:"+address+"\n"
                +"HourWorked:"+hourworked+"\n"+"Rate:"+rate+"\n"+"Payment"+pay()+"\n"+
                "=====================================================";
    }
    @Override
    public int compareTo(StaffMember o) {
        return name.compareTo(o.name);
    }
}
