package management;

public class Volunteer extends StaffMember implements Comparable<StaffMember> {

    public Volunteer(int id, String name, String address) {
        super(id, name, address);
    }

    public void setId(int id){ this.id=id; }
    public void setName(String name){this.name=name; }
    public void setAddress(String address){
        this.address=address;
    }
    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getAddress() {
        return address;
    }

    @Override
    public double pay() {
        return 0;
    }


    public  String toString(){
        return "ID:"+id+"\n"+"Name:"+name+"\n"+"Address:"+address+"\n"
                +"Thanks"+"\n"+
                "====================================================";
    }


    @Override
    public int compareTo(StaffMember o) {
        return name.compareTo(o.name);
    }
}