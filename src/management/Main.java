package management;

import java.util.Iterator;
import java.util.Scanner;
import java.util.TreeSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
public class Main {
    static Scanner sc = new Scanner(System.in);
    static String choice;
    private Pattern pt = Pattern.compile("\\d+");
    TreeSet<StaffMember> stMember = new TreeSet<>();
    public void add() {
        System.out.println("==================================================");
        System.out.print("1).Volunteer:  " + "\t");

        System.out.print("2).SalaryEmployee: " + "\t");

        System.out.print("3).HourlyEmployee: " + "\t");

        System.out.println("4).Back: ");

        System.out.println("---------------------------------------------------");
        System.out.print("-> Please Choose the Option above(1-4): ");
        Scanner q = new Scanner(System.in);

        switch (q.nextInt())
        {
            case 1:
                addVolunteer();
                break;
            case 2:
                addSalaryEmp();
                break;
            case 3:
                addHourlyEmp();
            case 4:
                chooseOp();
                System.out.println("-----Go Back To Menu-----");
            default:
                System.out.println("-> Choose Option(1-4) Pls enter the right option above: ");
                break;
        }
    }
    public void addVolunteer()  {
        System.out.println("==============INSERT INFO================");
        System.out.print("ID:");
        int id=sc.nextInt();
        sc.nextLine();
        System.out.print("NAME: ");
        String name=sc.nextLine();
        String nameUpp = name.substring(0,1).toUpperCase()+name.substring(1);
        System.out.print("ADDRESS: " );
        String address=sc.nextLine();
        StaffMember newStaff = new Volunteer(id,nameUpp,address);
        stMember.add(newStaff);
        System.out.println("Thank you!");
        System.out.println("VOLUNTEER is added successfully");

    }
    public void addSalaryEmp()  {
        System.out.println("==============INSERT INFO================");
        System.out.print("ID:");
        int id=sc.nextInt();
        sc.nextLine();
        System.out.print("NAME: ");
        String name=sc.nextLine();
        String nameUpp = name.substring(0,1).toUpperCase()+name.substring(1);
        System.out.print("ADDRESS: " );
        String address=sc.nextLine();
        System.out.print("SALARY: ");
        Double salary=sc.nextDouble();
        System.out.print("BONUS: " );
        Double bonus=sc.nextDouble();
        StaffMember newStaff = new SalaryEmployee(id,nameUpp,address,salary,bonus);
        stMember.add(newStaff);
        System.out.println("SALARYEMPLOYEE is added successfully");

    }
    public void addHourlyEmp()  {
        System.out.println("==============INSERT INFO================");
        System.out.print("ID:");
        int id=sc.nextInt();
        sc.nextLine();
        System.out.print("NAME: ");
        String name=sc.nextLine();
        String nameUpp = name.substring(0,1).toUpperCase()+name.substring(1);
        System.out.print("ADDRESS: " );
        String address=sc.nextLine();
        System.out.print("HOURWORKED: ");
        int hourworked=sc.nextInt();
        System.out.print("RATE: " );
        Double rate=sc.nextDouble();
        StaffMember newStaff = new HourlyEmployee(id,nameUpp,address,hourworked,rate);
        stMember.add(newStaff);
        System.out.println("HOURLYEMPLOYEE is added successfully");

    }
    public void edit()  {
        System.out.println("==============EDIT INFO================");
        System.out.print("=> ENTER STAFF'S ID TO UPDATE:");
        String sId = sc.next();

        Matcher matcher = pt.matcher(sId);
        boolean check = matcher.matches();
        if(check) {
           int id = Integer.parseInt(sId);
            Iterator<StaffMember> iterator = stMember.iterator();
            while (iterator.hasNext()) {
                StaffMember edit = iterator.next();
                if (edit.id == id) {
                    System.out.println(edit);
                    if (edit instanceof Volunteer) {
                        Volunteer editVolunteer = (Volunteer) edit;
                        editVolunteer(editVolunteer);
                    }else if (edit instanceof SalaryEmployee){
                        SalaryEmployee editSalaryEmp = (SalaryEmployee) edit;
                        editSalaryEmp(editSalaryEmp);
                    }else if(edit instanceof HourlyEmployee){
                        HourlyEmployee editHourlyEmp = (HourlyEmployee) edit;
                        editHourlyEmp(editHourlyEmp);
                    }else {
                        System.out.println("Invalid");
                    }
                }
            }
        }
    }
    public void editVolunteer(Volunteer editVolunteer)  {
        System.out.println("==============NEW INFO OF STAFF MEMBER================");
        System.out.print("=> ENTER STAFF'S NAME: ");
        sc.nextLine();
        String name=sc.nextLine();
        String nameUpp = name.substring(0,1).toUpperCase()+name.substring(1);
        System.out.print("=> ENTER STAFF'S ADDRESS: ");
        String address=sc.nextLine();
        StaffMember edit = new Volunteer(editVolunteer.id,nameUpp,address);
        stMember.add(edit);
        stMember.remove(editVolunteer);
        System.out.println("Volunteer is edit successfully");

    }
    public void editSalaryEmp(SalaryEmployee editSalaryEmp)  {
        System.out.println("==============NEW INFO OF STAFF MEMBER================");
        System.out.print("=> ENTER STAFF'S NAME: ");
        sc.nextLine();
        String name=sc.nextLine();
        String nameUpp = name.substring(0,1).toUpperCase()+name.substring(1);
        System.out.print("=> ENTER STAFF'S ADDRESS: " );
        String address=sc.nextLine();
        System.out.print("ENTER STAFF'S SALARY: ");
        Double salary=sc.nextDouble();
        System.out.print("ENTER STAFF'S BONUS: " );
        Double bonus=sc.nextDouble();
        StaffMember edit;
        edit = new SalaryEmployee(editSalaryEmp.id, nameUpp, address, salary, bonus);
        stMember.add(edit);
        stMember.remove(editSalaryEmp);
        System.out.println("EMPLOYEE is edit successfully");
    }
    public void editHourlyEmp(HourlyEmployee editHourlyEmp)  {
        System.out.println("==============NEW INFO OF STAFF MEMBER================");
        System.out.print("=> ENTER STAFF'S NAME: ");
        sc.nextLine();
        String name=sc.nextLine();
        String nameUpp = name.substring(0,1).toUpperCase()+name.substring(1);
        System.out.print("=> ENTER STAFF'S ADDRESS: " );
        String address=sc.nextLine();
        System.out.print("HOURWORKED: ");
        int hourworked=sc.nextInt();
        System.out.print("RATE: " );
        Double rate=sc.nextDouble();
        StaffMember edit = new HourlyEmployee(editHourlyEmp.id,nameUpp,address,hourworked,rate);
        stMember.add(edit);
        stMember.remove(editHourlyEmp);
        System.out.println("EMPLOYEE is edit successfully");

    }
    public void remove() {
        System.out.println("================DELETE INFO================");
        System.out.print("=> ENTER STAFF'S ID TO DELETE:");
        int id = sc.nextInt();
        Iterator<StaffMember> iterator = stMember.iterator();
        while (iterator.hasNext()) {
            StaffMember re = iterator.next();
            if (re.getId() == id)
                stMember.remove(re);
        }

        System.out.println("EMPLOYEE is delete successfully");

    }
    public void chooseOp() {
        System.out.println("1- Add Employee");
        System.out.println("2- Edit");
        System.out.println("3- Remove");
        System.out.println("4- Exit");
        System.out.println("-----------------------------------------------------");
        System.out.print("-> Please Choose the Option above(1-4): ");
        choice = sc.next();
    }

    public static void main(String[] args){
   Main sm = new Main();
   StaffMember m1 = new Volunteer(1,"Nara","PP");
   StaffMember m2 = new SalaryEmployee(2, "Thida", "KPS", 550.00, 50.00);
   StaffMember m3 = new HourlyEmployee(3, "Vatha", "BTB",80,15) ;

   sm.stMember.add(m1);
   sm.stMember.add(m2);
   sm.stMember.add(m3);

   while (true){
       for(StaffMember st:sm.stMember) {
           System.out.println(st);
       }
           sm.chooseOp();
           switch (sm.choice) {
               case "1":
                   sm.add();
                   break;
               case "2":
                   sm.edit();
                   break;
               case "3":
                   sm.remove();
                   break;
               case "4":
                   System.out.println(" => Choose option(1-4) : 4\n" +
                           "(^-^) Good Bye! (^-^)\n ");
                   System.exit(0);
                   break;
               default:
                   System.out.println("-> Choose Option(1-4) Pls enter the right option above: ");
                   break;
           }
       }
    }
  }
